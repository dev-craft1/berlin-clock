package org.kmt.berlinclock;

import java.time.LocalTime;
import java.time.format.DateTimeParseException;

class Feature1Solution {

    protected Feature1Solution() {}


    public static String getSingleMinuteRowString(String timeValue) {
        try {

            int mod = LocalTime.parse(timeValue).getMinute() % 5;
            return "Y".repeat(mod) + "O".repeat((4 - mod));
        }
        catch (DateTimeParseException | NullPointerException exception) {
            return "Malformatted time error: ".concat(exception.getMessage());
        }
    }

    public static String getFiveMinutesRowString(String timeValue) {

        try {

            int divResult = LocalTime.parse(timeValue).getMinute();
            StringBuilder result = new StringBuilder();

            if(divResult >= 5) {
                int column = 1;
                for (int cpt = 5; cpt <= divResult; cpt+=5) {
                    if(column % 3 == 0) {
                        result.append('R');
                    }
                    else {
                        result.append('Y');
                    }
                    ++column;
                }
            }
            return result.toString().concat("O".repeat(11- (divResult/5) ));
        }
        catch (DateTimeParseException | NullPointerException exception) {
            return "Malformatted time error: ".concat(exception.getMessage());
        }

    }

    public static String getSingleHourRowString(String timeValue) {
        try {

            int mod = LocalTime.parse(timeValue).getHour() % 5;
            return "R".repeat(mod) + "O".repeat( (4 - mod) );
        }
        catch (DateTimeParseException | NullPointerException exception) {
            return "Malformatted time error: ".concat(exception.getMessage());
        }
    }

    public static String getFiveHoursRowString(String timeValue) {
        try {

            int mod = LocalTime.parse(timeValue).getHour() / 5;
            return "R".repeat(mod) + "O".repeat((4 - mod));
        }
        catch (DateTimeParseException | NullPointerException exception) {
            return "Malformatted time error: ".concat(exception.getMessage());
        }
    }

    public static String getSecondsLampe(String timeValue) {
        try {

            int seconds = LocalTime.parse(timeValue).getSecond();
            return seconds % 2 == 0 ? "Y" : "O";
        }
        catch (DateTimeParseException | NullPointerException exception) {
            return "Malformatted time error: ".concat(exception.getMessage());
        }
    }

    public static String entiredBerlinClock(String timeValue) {
        try {

            return getSecondsLampe(timeValue) + getFiveHoursRowString(timeValue) + getSingleHourRowString(timeValue)
                    + getFiveMinutesRowString(timeValue) + getSingleMinuteRowString(timeValue);
        }
        catch (Exception exception) {
            return "Malformatted time error: ".concat(exception.getMessage());
        }
    }

}
