package org.kmt.berlinclock;

public class MainApp {
    public static void main(String[] args) {

        System.out.println("--------- getSingleMinuteRowString");
        System.out.println(Feature1Solution.getSingleMinuteRowString("00:00:00"));
        System.out.println(Feature1Solution.getSingleMinuteRowString("23:59:59"));
        System.out.println(Feature1Solution.getSingleMinuteRowString("12:32:00"));
        System.out.println(Feature1Solution.getSingleMinuteRowString("12:34:00"));
        System.out.println(Feature1Solution.getSingleMinuteRowString("12:35:00"));

        System.out.println("--------- getFiveMinutesRowString");

        System.out.println(Feature1Solution.getFiveMinutesRowString("00:00:00"));
        System.out.println(Feature1Solution.getFiveMinutesRowString("23:59:59"));
        System.out.println(Feature1Solution.getFiveMinutesRowString("12:04:00"));
        System.out.println(Feature1Solution.getFiveMinutesRowString("12:23:00"));
        System.out.println(Feature1Solution.getFiveMinutesRowString("12:35:00"));

        System.out.println("--------- getSingleHourRowString");
        System.out.println(Feature1Solution.getSingleHourRowString("00:00:00"));
        System.out.println(Feature1Solution.getSingleHourRowString("23:59:59"));
        System.out.println(Feature1Solution.getSingleHourRowString("02:04:00"));
        System.out.println(Feature1Solution.getSingleHourRowString("08:23:00"));
        System.out.println(Feature1Solution.getSingleHourRowString("14:35:00"));

        System.out.println("--------- getFiveHoursRowString");
        System.out.println(Feature1Solution.getFiveHoursRowString("00:00:00"));
        System.out.println(Feature1Solution.getFiveHoursRowString("23:59:59"));
        System.out.println(Feature1Solution.getFiveHoursRowString("02:04:00"));
        System.out.println(Feature1Solution.getFiveHoursRowString("08:23:00"));
        System.out.println(Feature1Solution.getFiveHoursRowString("16:35:00"));

        System.out.println("--------- getSecondsLampe");
        System.out.println(Feature1Solution.getSecondsLampe("00:00:00"));
        System.out.println(Feature1Solution.getSecondsLampe("23:59:59"));

        System.out.println("--------- entiredBerlinClock");
        System.out.println(Feature1Solution.entiredBerlinClock("00:00:00"));
        System.out.println(Feature1Solution.entiredBerlinClock("23:59:59"));
        System.out.println(Feature1Solution.entiredBerlinClock("16:50:06"));
        System.out.println(Feature1Solution.entiredBerlinClock("11:37:01"));
    }
}
